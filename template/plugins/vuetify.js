import Vue from 'vue'
{{#alacarte}}
import {
  Vuetify,
  VApp,
  VCard,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar
} from 'vuetify'

Vue.use(Vuetify, {
  components: {
    VApp,
    VCard,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar
  }{{#theme}},
  theme: {
    primary: '#9c27b0',
    accent: '#ce93d8',
    secondary: '#424242',
    info: '#0D47A1',
    warning: '#ffb300',
    error: '#B71C1C',
    success: '#2E7D32'
  }{{/theme}}
})
{{else}}
import Vuetify from 'vuetify'

Vue.use(Vuetify, { 
  iconfont: 'md', // 'md' || 'mdi' || 'fa' || 'fa4'
  theme: {
    primary: '#001f3f',
    secondary: '#39cccc',
    accent: '#f012be',
    success: '#2ecc40',
    info: '#7fdbff',
    warning: '#ffdc00',
    error: '#85144b'
}})
{{/alacarte}}
